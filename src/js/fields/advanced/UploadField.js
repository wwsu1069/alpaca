(function ($) {
  var Alpaca = $.alpaca;

  Alpaca.Fields.UploadField = Alpaca.ControlField.extend(
    /**
     * @lends Alpaca.Fields.UploadField.prototype
     */
    {
      /**
       * @constructs
       * @augments Alpaca.Fields.ControlField
       *
       * @class File upload control that can be mounted on top of "object" or "array" types.
       *
       * @param {Object} container Field container.
       * @param {Any} data Field data.
       * @param {Object} options Field options.
       * @param {Object} schema Field schema.
       * @param {Object|String} view Field view.
       * @param {Alpaca.Connector} connector Field connector.
       */
      constructor: function (
        container,
        data,
        options,
        schema,
        view,
        connector
      ) {
        var self = this;

        this.base(container, data, options, schema, view, connector);

        this.isArrayType = function () {
          return self.schema.type === "array";
        };

        this.isObjectType = function () {
          return self.schema.type === "object";
        };

        // wraps an existing template descriptor into a method that looks like fn(model)
        // this is compatible with the requirements of fileinput
        // config looks like
        //    {
        //       "files": [],
        //       "formatFileSize": fn,
        //       "options": {}
        //    }
        //

        this.wrapTemplate = function (templateId) {
          return function (config) {
            var files = config.files;
            var formatFileSize = config.formatFileSize;
            var options = config.options;

            var rows = [];
            for (var i = 0; i < files.length; i++) {
              var model = {};
              model.options = self.options;
              model.file = Alpaca.cloneObject(files[i]);
              model.size = formatFileSize(model.size);
              model.buttons = self.options.buttons;
              model.view = self.view;
              model.fileIndex = i;

              var row = Alpaca.tmpl(
                self.view.getTemplateDescriptor(templateId),
                model,
                self
              );

              rows.push(row[0]);
            }

            rows = $(rows);
            $(rows).each(function () {
              if (options.dropzone && options.dropzone.autoUpload) {
                // disable start button
                $(this).find("button.start").css("display", "none");
              }

              self.handleWrapRow(this, options);
            });

            return $(rows);
          };
        };
      },

      /**
       * @see Alpaca.ControlField#getFieldType
       */
      getFieldType: function () {
        return "upload";
      },

      /**
       * @see Alpaca.Fields.TextField#setup
       */
      setup: function () {
        var self = this;

        this.base();

        // disable bottom control buttons (we have a conflict over the 'buttons' namespace)
        self.options.renderButtons = false;

        if (!self.options.buttons) {
          self.options.buttons = [];
        }

        if (!self.options.hideDeleteButton) {
          self.options.buttons.push({
            key: "delete",
            isDelete: true,
          });
        }

        if (typeof self.options.showUploadPreview === "undefined") {
          self.options.showUploadPreview = true;
        }

        if (typeof self.options.showHeaders === "undefined") {
          self.options.showHeaders = true;
        }

        if (!self.data) {
          self.data = [];
        }

        // convert to array if not array already
        if (self.data && Alpaca.isObject(self.data)) {
          self.data = [self.data];
        }

        // upload
        if (!self.options.upload) {
          self.options.upload = {};
        }

        // support copying back the maxNumberOfFiles from the upload plugin's settings
        if (typeof self.options.maxNumberOfFiles === "undefined") {
          if (typeof self.options.upload.maxNumberOfFiles !== "undefined") {
            self.options.maxNumberOfFiles =
              self.options.upload.maxNumberOfFiles;
          }
        }

        // figure out reasonable maxNumberOfFiles
        if (typeof self.options.maxNumberOfFiles === "undefined") {
          self.options.maxNumberOfFiles = 1;
          if (self.isArrayType()) {
            self.options.maxNumberOfFiles = -1;
          }
        }

        // safe guard
        if (self.isObjectType()) {
          self.options.maxNumberOfFiles = 1;
        }

        if (self.options.multiple === false) {
          self.options.maxNumberOfFiles = 1;
        }

        if (
          self.options.maxNumberOfFiles > 1 ||
          self.options.maxNumberOfFiles === -1
        ) {
          self.options.multiple = true;
        }

        // copy setting into upload plugin config
        self.options.upload.maxNumberOfFiles = 9999;
        if (self.options.maxNumberOfFiles > 0) {
          self.options.upload.maxNumberOfFiles = self.options.maxNumberOfFiles;
        }

        // Backwards compatibility fileUpload -> Dropzone
        if (
          typeof self.options.maxFiles === "undefined" &&
          typeof self.options.maxNumberOfFiles !== "undefined"
        )
          self.options.maxFiles = self.options.maxNumberOfFiles;

        // max file size
        if (typeof self.options.maxFileSize === "undefined") {
          if (self.options.upload.maxFileSize) {
            self.options.maxFileSize = self.options.upload.maxFileSize;
          } else {
            self.options.maxFileSize = -1; // no limit
          }

          // copy setting into upload
          if (self.options.maxFileSize) {
            self.options.upload.maxFileSize = self.options.maxFileSize;
          }
        }

        // Backwards compatibility fileUpload -> Dropzone
        if (
          typeof self.options.maxFilesize === "undefined" &&
          typeof self.options.maxFileSize !== "undefined"
        )
          self.options.maxFilesize = self.options.maxFileSize;

        // file types
        if (typeof self.options.fileTypes === "undefined") {
          if (self.options.upload.acceptFileTypes) {
            self.options.fileTypes = self.options.upload.acceptFileTypes;
          } else {
            self.options.fileTypes = null; // no restrictions
          }

          // copy setting into upload
          if (self.options.fileTypes) {
            self.options.upload.acceptFileTypes = self.options.fileTypes;
          }
        }

        // Backwards compatibility fileUpload -> Dropzone
        if (
          typeof self.options.acceptedFiles === "undefined" &&
          typeof self.options.fileTypes !== "undefined"
        )
          self.options.acceptedFiles = self.options.fileTypes;

        // if Alpaca is configured for CSRF support and a CSRF cookie or token is available,
        // then apply it to the headers that are sent over the wire via the underlying ajax
        // for the file upload control

        // if we have a CSRF token, apply it to the headers
        var csrfToken = self.determineCsrfToken();
        if (csrfToken) {
          if (!self.options.upload) {
            self.options.upload = {};
          }

          if (!self.options.upload.headers) {
            self.options.upload.headers = {};
          }

          self.options.upload.headers[Alpaca.CSRF_HEADER_NAME] = csrfToken;
        }
      },

      determineCsrfToken: function () {
        // is there a direct token specified?
        var csrfToken = Alpaca.CSRF_TOKEN;
        if (!csrfToken) {
          // is there a cookie that we can pull the value from?
          for (var t = 0; t < Alpaca.CSRF_COOKIE_NAMES.length; t++) {
            var cookieName = Alpaca.CSRF_COOKIE_NAMES[t];

            var cookieValue = Alpaca.readCookie(cookieName);
            if (cookieValue) {
              csrfToken = cookieValue;
              break;
            }
          }
        }

        return csrfToken;
      },

      prepareControlModel: function (callback) {
        var self = this;

        self.base(function (model) {
          model.chooseButtonLabel = self.options.chooseButtonLabel;
          if (!model.chooseButtonLabel) {
            model.chooseButtonLabel = self.getMessage("chooseFiles");
            if (self.options.maxNumberOfFiles === 1) {
              model.chooseButtonLabel = self.getMessage("chooseFile");
            }
          }

          model.dropZoneMessage = self.options.dropZoneMessage;
          if (!model.dropZoneMessage) {
            model.dropZoneMessage = self.getMessage("dropZoneMultiple");
            if (model.options.maxNumberOfFiles === 1) {
              model.dropZoneMessage = self.getMessage("dropZoneSingle");
            }

            if (model.options.directory) {
              model.dropZoneMessage = self.getMessage(
                "dropZoneMultipleDirectory"
              );
            }
          }

          model.selectFromExistingMessage =
            self.options.selectFromExistingMessage;
          if (!model.selectFromExistingMessage) {
            model.selectFromExistingMessage = self.getMessage(
              "selectFromExistingMultiple"
            );
            if (model.options.maxNumberOfFiles === 1) {
              model.selectFromExistingMessage = self.getMessage(
                "selectFromExistingSingle"
              );
            }
          }

          callback(model);
        });
      },

      afterRenderControl: function (model, callback) {
        var self = this;

        this.base(model, function () {
          self.handlePostRender(function () {
            // if we're in display-only mode, we hide a bunch of things
            if (self.isDisplayOnly()) {
              $(self.control).find("button").hide();
              $(self.control).find(".btn").hide();
              $(self.control).find(".alpaca-fileupload-chooserow").hide();
              $(self.control).find(".dropzone-message").hide();
            }

            callback();
          });
        });
      },

      /**
       * Gets the upload template.
       */
      getUploadTemplate: function () {
        //return this.wrapTemplate("control-upload-partial-upload");
        return `<div class="row mt-2 template-upload">
        <div class="col-3">
                <span class="preview"><img
                    src=""
                    alt=""
                    data-dz-thumbnail=""
                /></span>
        </div>
        <div class="col-3">
            <p class="mb-0">
                <span class="lead" data-dz-name=""></span>
                <strong class="error text-danger" data-dz-errormessage=""></strong>
            </p>
        </div>
        <div class="col-3">
          <span data-dz-size=""><strong></strong></span>
        </div>
        <div class="col-3">
              <div
                  class="progress progress-striped active w-100"
                  role="progressbar"
                  aria-valuemin="0"
                  aria-valuemax="100"
                  aria-valuenow="0"
              >
                  <div
                  class="progress-bar progress-bar-success"
                  style="width:0%;"
                  data-dz-uploadprogress=""
                  ></div>
              </div>
              <div class="btn-group">
                    <button data-dz-remove="" class="btn btn-warning cancel">
                        <i class="fas fa-times-circle"></i>
                        <span>Cancel</span>
                    </button>
                    <button data-dz-remove="" class="btn btn-danger delete">
                        <i class="fas fa-trash"></i>
                        <span>Delete</span>
                    </button>
              </div>
        </div>
      </div>`;
      },

      /**
       * Gets the download template.
       */
      getDownloadTemplate: function () {
        return this.wrapTemplate("control-upload-partial-download");
      },

      handlePostRender: function (callback) {
        var self = this;

        var el = this.control;

        // file upload config
        var fileUploadConfig = {};

        // defaults
        // fileUploadConfig["dataType"] = "json";
        // fileUploadConfig["uploadTemplateId"] = null;

        fileUploadConfig["previewTemplate"] = this.getUploadTemplate();
        // TODO: figure out what to do. The above returns a function, but Dropzone expects a string.
        // However, we need the Alpaca template for it to function correctly with file descriptors.
        // It looks like we have no choice though and will need to find a way to "hack" file descriptors.

        // fileUploadConfig["downloadTemplateId"] = null;
        // fileUploadConfig["downloadTemplate"] = this.getDownloadTemplate();
        // fileUploadConfig["previewsContainer"] = ".files";
        // fileUploadConfig["dropZone"] = $(el).find(".fileupload-active-zone");
        // fileUploadConfig["clickable"] = ".fileinput-button";
        fileUploadConfig["url"] = "/";
        fileUploadConfig["method"] = "post";
        fileUploadConfig["disablePreviews"] = !self.options.showUploadPreview;
        fileUploadConfig["acceptedFiles"] =
          self.options.acceptedFiles || "image/*";
        fileUploadConfig["maxFilesize"] =
          self.options.maxFilesize || 1024 * 1024 * 8;
        fileUploadConfig["autoProcessQueue"] = true;
        fileUploadConfig["addRemoveLinks"] = true;
        fileUploadConfig["params"] = (files, xhr, chunk) => {
          if (chunk) {
            return Object.assign(
              {
                dzuuid: chunk.file.upload.uuid,
                dzchunkindex: chunk.index,
                dztotalfilesize: chunk.file.size,
                dzchunksize: this.options.chunkSize,
                dztotalchunkcount: chunk.file.upload.totalChunkCount,
                dzchunkbyteoffset: chunk.index * this.options.chunkSize,
              },
              self.options.upload.formData
            );
          } else {
            return self.options.upload.formData;
          }
        };

        if (self.options.upload) {
          for (var k in self.options.upload) {
            fileUploadConfig[k] = self.options.upload[k];
          }
        }

        if (self.options.multiple) {
          $(el).find(".alpaca-fileupload-input").attr("multiple", true);
        }

        if (self.options.directory) {
          $(el).find(".alpaca-fileupload-input").attr("directory", true);
          //$(el).find(".alpaca-fileupload-input").attr("webkitdirectory", true);
        }

        if (self.options.name) {
          $(el)
            .find(".alpaca-fileupload-input")
            .attr("name", self.options.name);
        }

        // hide the progress bar at first
        $(el).find(".progress").css("display", "none");

        // allow for extension
        self.applyConfiguration(fileUploadConfig);

        // instantiate the control
        self.fileUpload = $(el).dropzone(fileUploadConfig);
        self.fileUpload = self.fileUpload[0];
        self.fileUpload = self.fileUpload.dropzone;

        // Add the total upload progress event handler
        if (self.options.totaluploadprogress) {
          self.fileUpload.on(
            "totaluploadprogress",
            self.options.totaluploadprogress
          );
        }

        // some limit checks
        self.fileUpload.on("addedfile", function (file) {
          var uploadErrors = [];

          var i = 0;

          var bad = false;

          // general "before add" validation function
          if (self.options.beforeAddValidator) {
            var errorMessage = self.options.beforeAddValidator(file);
            if (Alpaca.isString(errorMessage) || errorMessage === false) {
              if (!Alpaca.isString(errorMessage)) {
                errorMessage = "Not an accepted file: " + file.name;
              }

              uploadErrors.push(errorMessage);

              bad = true;
            }
          }

          if (uploadErrors.length > 0) {
            self.options.errorHandler(uploadErrors);
          }

          self.refreshValidationState(true);
          self.refreshUIState();
        });

        self.fileUpload.on("removedfile", function (file) {
          self.onFileDelete.call(self, file);

          // remove from property value
          if (self.isArrayType()) {
            var array = self.getValueAsArray();
            var fileIndex = array.findIndex((f) => f === file.name);

            if (fileIndex) {
              array.splice(fileIndex, 1);
              self.setValueAsArray(array);
            }
          } else if (self.isObjectType()) {
            self.setValueAsArray([]);
          }

          self.triggerWithPropagation("change");
          setTimeout(function () {
            self.refreshUIState();

            if (self.options.afterFileUploadRemove) {
              self.options.afterFileUploadRemove.call(self, row);
            }
          }, 200);
        });

        self.fileUpload.on("sending", function (file, xhr) {
          if (self.options.beforeFileUploadSubmitHandler) {
            self.options.beforeFileUploadSubmitHandler.call(self, file, xhr);
          }
        });

        /**
         * When files are uploaded, we adjust the value of the field.
         */
        self.fileUpload.on("success", function (file, responseText) {
          if (!responseText.files || !responseText.files.length) {
            self.onUploadFail(file, "Unknown error");

            if (self.options.afterFileUploadFail) {
              self.options.afterFileUploadFail.call(
                self,
                file,
                "Unknown error"
              );
            }

          } else {
            self.convertFileToDescriptor(
              responseText.files[0],
              function (err, descriptor) {
                var array = self.getValueAsArray();

                if (descriptor) {
                  array.push(descriptor);
                }

                self.setValueAsArray(array);

                if (self.options.afterFileUploadDone) {
                  self.options.afterFileUploadDone.call(self, file);
                }
              }
            );
          }
        });

        /**
         * When file uploads fail, alert...
         */
        self.fileUpload.on("error", function (file, message) {
          self.onUploadFail(file, message);

          if (self.options.afterFileUploadFail) {
            self.options.afterFileUploadFail.call(self, file, message);
          }
        });

        /**
         * Whether success or fail, we handle the results.
         */
        self.fileUpload.on("complete", function (file) {
          self.refreshUIState();

          if (self.options.afterFileUploadAlways) {
            self.options.afterFileUploadAlways.call(self, file);
          }
        });

        // allow for extension
        self.applyBindings(self.fileUpload, el);

        // allow for preloading of documents
        self.preload(self.fileUpload, el, function (files) {
          if (files && files.length > 0) {
            files.forEach(function (file) {
              self.fileUpload.displayExistingFile(file, file.thumbnailUrl);
            });
            self.afterPreload(self.fileUpload, el, files, function () {
              callback();
            });
          } else {
            callback();
          }
        });

        /*
        if (typeof document !== "undefined") {
          document.querySelector("#actions .start").onclick = function () {
            fileUpload.processQueue();
          };
        }
        */
      },

      handleWrapRow: function (row, options) {},

      applyTokenSubstitutions: function (text, index, file) {
        // DEPRECATED
      },

      /**
       * Extension point for adding properties and callbacks to the file upload config.
       *
       * @param fileUploadconfig
       */
      applyConfiguration: function (fileUploadconfig) {},

      /**
       * Extension point for binding event handlers to file upload instance.
       *
       * @param fileUpload
       */
      applyBindings: function (fileUpload) {},

      /**
       * Converts from a file to a storage descriptor.
       *
       * A descriptor looks like:
       *
       *      {
       *          "id": ""
       *          ...
       *      }
       *
       * A descriptor may contain additional properties as needed by the underlying storage implementation
       * so as to retrieve metadata about the described file.
       *
       * Assumption is that the underlying persistence mechanism may need to be consulted.  Thus, this is async.
       *
       * By default, the descriptor mimics the file.
       *
       * @param file
       * @param callback function(err, descriptor)
       */
      convertFileToDescriptor: function (file, callback) {
        var descriptor = {
          id: file.id,
          name: file.id, // Also use id for name as it is easiest for matching descriptors
          size: file.size,
          url: file.url,
          thumbnailUrl: file.thumbnailUrl,
          deleteUrl: file.deleteUrl,
          deleteType: file.deleteType,
        };

        callback(null, descriptor);
      },

      /**
       * Converts a storage descriptor to a file.
       *
       * A file looks like:
       *
       *      {
       *          "id": "",
       *          "name": "picture1.jpg",
       *          "size": 902604,
       *          "url": "http:\/\/example.org\/files\/picture1.jpg",
       *          "thumbnailUrl": "http:\/\/example.org\/files\/thumbnail\/picture1.jpg",
       *          "deleteUrl": "http:\/\/example.org\/files\/picture1.jpg",
       *          "deleteType": "DELETE"
       *      }
       *
       * Since an underlying storage mechanism may be consulted, an async callback hook is provided.
       *
       * By default, the descriptor mimics the file.
       *
       * @param descriptor
       * @param callback function(err, file)
       */
      convertDescriptorToFile: function (descriptor, callback) {
        var file = {
          id: descriptor.id,
          name: descriptor.id, // Also use id for name as it is easiest for matching descriptors
          size: descriptor.size,
          url: descriptor.url,
          thumbnailUrl: descriptor.thumbnailUrl,
          deleteUrl: descriptor.deleteUrl,
          deleteType: descriptor.deleteType,
        };

        callback(null, file);
      },

      /**
       * Extension point for "enhancing" data received from the remote server after uploads have been submitted.
       * This provides a place to convert the data.rows back into the format which the upload control expects.
       *
       * Expected format:
       *
       *    data.result.rows = [{...}]
       *    data.result.files = [{
       *      "id": "",
       *      "path": "",
       *      "name": "picture1.jpg",
       *      "size": 902604,
       *      "url": "http:\/\/example.org\/files\/picture1.jpg",
       *      "thumbnailUrl": "http:\/\/example.org\/files\/thumbnail\/picture1.jpg",
       *      "deleteUrl": "http:\/\/example.org\/files\/picture1.jpg",
       *      "deleteType": "DELETE"*
       *    }]
       *
       * @param fileUploadConfig
       * @param data
       */
      enhanceFiles: function (fileUploadConfig, data) {
        // DEPRECATED
      },

      /**
       * Preloads data descriptors into files.
       *
       * @param fileUpload
       * @param el
       * @param callback
       */
      preload: function (fileUpload, el, callback) {
        var self = this;

        var files = [];

        // now preload with files based on property value
        var descriptors = self.getValueAsArray();

        var f = function (i) {
          if (i == descriptors.length) {
            // jshint ignore:line
            // all done
            return callback(files);
          }

          self.convertDescriptorToFile(descriptors[i], function (err, file) {
            if (file) {
              files.push(file);
            }

            f(i + 1);
          });
        };
        f(0);
      },

      afterPreload: function (fileUpload, el, files, callback) {
        var self = this;

        self.refreshUIState();

        callback();
      },

      /**
       * @see Alpaca.Fields.ControlField#getControlValue
       */
      getControlValue: function () {
        return this.data;
      },

      /**
       * Hands back the value as either an object or array, depending on the schema type.
       *
       * @returns {*}
       */
      getValue: function () {
        var value = this.data;

        if (this.isObjectType()) {
          if (this.data && this.data.length > 0) {
            value = this.data[0];
          } else {
            value = undefined;
          }
        }

        return value;
      },

      setValue: function (value) {
        if (!value) {
          this.data = [];
        } else {
          if (Alpaca.isArray(value)) {
            this.data = value;
          } else if (Alpaca.isObject(value)) {
            this.data = [value];
          }
        }

        this.updateObservable();

        this.triggerUpdate();
      },

      /**
       * @returns {Array} the value as an array
       */
      getValueAsArray: function () {
        return this.data || [];
      },

      /**
       * Sets the value as an array.
       *
       * @param array
       */
      setValueAsArray: function (array) {
        var self = this;

        if (self.isArrayType()) {
          self.setValue(array);
        } else if (self.isObjectType()) {
          var val = null;
          if (array && array.length > 0) {
            val = array[0];
          }

          self.setValue(val);
        }
      },

      reload: function (callback) {
        var self = this;

        var descriptors = this.getValueAsArray();

        var files = [];

        var f = function (i) {
          if (i === descriptors.length) {
            // jshint ignore:line
            // all done

            if (files && files.length > 0) {
              files.forEach(function (file) {
                self.fileUpload.displayExistingFile(file, file.thumbnailUrl);
              });
            }

            // refresh validation state
            self.refreshValidationState();

            return callback();
          }

          self.convertDescriptorToFile(descriptors[i], function (err, file) {
            if (file) {
              files.push(file);
            }
            f(i + 1);
          });
        };
        f(0);
      },

      plugin: function () {
        var self = this;

        return self.fileUpload;
      },

      refreshUIState: function () {
        var self = this;

        var fileUpload = self.plugin();
        if (fileUpload) {
          var maxNumberOfFiles = self.options.maxNumberOfFiles;

          if (maxNumberOfFiles === -1) {
            self.refreshButtons(true);
          } else {
            if (fileUpload.files.length >= maxNumberOfFiles) {
              self.refreshButtons(false);
            } else {
              self.refreshButtons(true);
            }
          }
        }
      },

      refreshButtons: function (enabled) {
        var self = this;

        // disable select files button
        $(self.control).find(".btn.fileinput-button").prop("disabled", true);
        $(self.control)
          .find(".btn.fileinput-button")
          .attr("disabled", "disabled");

        // hide dropzone message
        $(self.control)
          .find(".fileupload-active-zone p.dropzone-message")
          .css("display", "none");

        if (enabled) {
          // enable select files button
          $(self.control).find(".btn.fileinput-button").prop("disabled", false);
          $(self.control).find(".btn.fileinput-button").attr("disabled", null);

          // show dropzone message
          $(self.control)
            .find(".fileupload-active-zone p.dropzone-message")
            .css("display", "block");
        }
      },

      onFileDelete: function (file) {
        var self = this;

        var deleteUrl = file.deleteUrl;
        var deleteMethod = file.deleteType;

        var c = {
          method: deleteMethod,
          url: deleteUrl,
          headers: {},
        };

        var csrfToken = self.determineCsrfToken();
        if (csrfToken) {
          c.headers[Alpaca.CSRF_HEADER_NAME] = csrfToken;
        }

        $.ajax(c);
      },

      onUploadFail: function (file, message) {
        var self = this;

        if (self.options.errorHandler) {
          self.options.errorHandler([message]);
        }

        console.error(
          new Error(`Failed to upload file ${file.name}: ${message}.`)
        );
      },

      /**
       * @see Alpaca.Field#disable
       */
      disable: function () {
        // disable select button
        $(this.field).find(".fileinput-button").prop("disabled", true);
        $(this.field).find(".fileinput-button").attr("disabled", "disabled");

        // hide the upload well
        $(this.field)
          .find(".alpaca-fileupload-well")
          .css("visibility", "hidden");
      },

      /**
       * @see Alpaca.Field#enable
       */
      enable: function () {
        $(this.field).find(".fileinput-button").prop("disabled", false);
        $(this.field).find(".fileinput-button").removeAttr("disabled");

        // show the upload well
        $(this.field)
          .find(".alpaca-fileupload-well")
          .css("visibility", "visible");
      },

      /* builder_helpers */

      /**
       * @see Alpaca.ControlField#getTitle
       */
      getTitle: function () {
        return "Upload Field";
      },

      /**
       * @see Alpaca.ControlField#getDescription
       */
      getDescription: function () {
        return "Provides an upload field with support for thumbnail preview";
      },

      /**
       * @see Alpaca.ControlField#getType
       */
      getType: function () {
        return "array";
      },

      /**
       * @private
       * @see Alpaca.Fields.TextField#getSchemaOfSchema
       */
      getSchemaOfOptions: function () {
        return Alpaca.merge(this.base(), {
          properties: {
            maxNumberOfFiles: {
              title: "Maximum Number of Files",
              description:
                "The maximum number of files to allow to be uploaded.  If greater than zero, the maximum number will be constrained.  If -1, then no limit is imposed.",
              type: "number",
              default: 1,
            },
            maxFileSize: {
              title: "Maximum File Size (in bytes)",
              description:
                "The maximum file size allowed per upload.  If greater than zero, the maximum file size will be limited to the given size in bytes.  If -1, then no limit is imposed.",
              type: "number",
              default: -1,
            },
            fileTypes: {
              title: "File Types",
              description:
                "A regular expression limiting the file types that can be uploaded based on filename",
              type: "string",
            },
            multiple: {
              title: "Multiple",
              description:
                "Whether to allow multiple file uploads.  If maxNumberOfFiles is not specified, multiple will toggle between 1 and unlimited.",
              type: "boolean",
              default: false,
            },
            directory: {
              title: "Directory",
              description:
                "Whether to allow directories (folders) to be dropped into the control for multi-document upload.",
              type: "boolean",
              default: false,
            },
            showUploadPreview: {
              title: "Show Upload Preview",
              description:
                "Whether to show thumbnails for uploaded assets (requires preview support)",
              type: "boolean",
              default: true,
            },
            errorHandler: {
              title: "Error Handler",
              description:
                "Optional function handler to be called when one or more files fails to upload.  This function is responsible for parsing the underlying xHR request and populating the error message state.",
              type: "function",
            },
          },
        });
      },

      /* end_builder_helpers */
    }
  );

  Alpaca.registerFieldClass("upload", Alpaca.Fields.UploadField);

  Alpaca.registerMessages({
    chooseFile: "Choose File...",
    chooseFiles: "Choose Files...",
    dropZoneSingle:
      "Click the Choose button or Drag and Drop a file here to upload...",
    dropZoneMultiple:
      "Click the Choose button or Drag and Drop files here to upload...",
    dropZoneMultipleDirectory:
      "Click the Choose button or Drag and Drop files or a folder here to upload...",
  });

  // https://github.com/private-face/jquery.bind-first/blob/master/dev/jquery.bind-first.js
  // jquery.bind-first.js
  (function ($) {
    var splitVersion = $.fn.jquery.split(".");
    var major = parseInt(splitVersion[0]);
    var minor = parseInt(splitVersion[1]);

    var JQ_LT_17 = major < 1 || (major === 1 && minor < 7);

    function eventsData($el) {
      return JQ_LT_17 ? $el.data("events") : $._data($el[0]).events;
    }

    function moveHandlerToTop($el, eventName, isDelegated) {
      var data = eventsData($el);
      var events = data[eventName];

      if (!JQ_LT_17) {
        var handler = isDelegated
          ? events.splice(events.delegateCount - 1, 1)[0]
          : events.pop();
        events.splice(isDelegated ? 0 : events.delegateCount || 0, 0, handler);

        return;
      }

      if (isDelegated) {
        data.live.unshift(data.live.pop());
      } else {
        events.unshift(events.pop());
      }
    }

    function moveEventHandlers($elems, eventsString, isDelegate) {
      var events = eventsString.split(/\s+/);
      $elems.each(function () {
        for (var i = 0; i < events.length; ++i) {
          var pureEventName = $.trim(events[i]).match(/[^\.]+/i)[0];
          moveHandlerToTop($(this), pureEventName, isDelegate);
        }
      });
    }

    $.fn.bindFirst = function () {
      var args = $.makeArray(arguments);
      var eventsString = args.shift();

      if (eventsString) {
        $.fn.bind.apply(this, arguments);
        moveEventHandlers(this, eventsString);
      }

      return this;
    };
  })($);
})(jQuery);
